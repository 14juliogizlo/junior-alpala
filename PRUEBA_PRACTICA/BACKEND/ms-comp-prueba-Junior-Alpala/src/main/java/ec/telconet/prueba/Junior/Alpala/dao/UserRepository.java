package ec.telconet.prueba.Junior.Alpala.dao;


import ec.telconet.prueba.Junior.Alpala.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findByUsuario(String username);

    Boolean existsByUsuario(String username);
}