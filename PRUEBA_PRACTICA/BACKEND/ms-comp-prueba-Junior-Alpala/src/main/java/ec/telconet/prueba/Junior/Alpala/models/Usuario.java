package ec.telconet.prueba.Junior.Alpala.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "admi_usuario")
@Data
@NoArgsConstructor
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 200)
    private String usuario;

    @Column(length = 200)
    private String password;

    @Column(length = 200)
    private String nombres;

    @Column(length = 200)
    private String apellidos;

    @Column(length = 200)
    private String direccion;

    @Column(length = 200)
    private String telefono;

    @Column(length = 200, name = "usuario_creacion")
    private String usuarioCreacion;

    @CreationTimestamp
    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion;

    @Column
    private byte[] foto;

    @Transient
    private Set<Rol> roles;

    public Usuario(String usuario, String password, String nombres, String apellidos, String direccion, String telefono, String usuarioCreacion, byte[] foto) {
        this.usuario = usuario;
        this.password = password;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.usuarioCreacion = usuarioCreacion;
        this.foto = foto;
    }
}