package ec.telconet.prueba.Junior.Alpala.services;

import ec.telconet.prueba.Junior.Alpala.models.InformacionUsuario;
import ec.telconet.prueba.Junior.Alpala.models.Usuario;

public interface InformacionUsuarioService {

    InformacionUsuario save(InformacionUsuario informacionUsuario);

    InformacionUsuario findByUsuario(Usuario usuario);

}
