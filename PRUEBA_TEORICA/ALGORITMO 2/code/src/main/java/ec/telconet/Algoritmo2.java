package ec.telconet;


import java.util.Arrays;
import java.util.Scanner;

/**
 * Clase que contiene el algoritmo 2
 * @author Saúl Alpala
 */
public class Algoritmo2 {

    public static void main(String[] args) {
        int[] moves = {1, 2, -1, 1, 0, 1, 2, -1, -1, -2};
//        int[] moves = inputMoves();

        char[][] board = createChessBoard();
        int currentX = 0;
        int currentY = 0;

        for (int i = 0; i < moves.length; i += 2) {
            int horizontalMove = moves[i];
            int verticalMove = moves[i + 1];

            int newX = currentX + horizontalMove;
            int newY = currentY + verticalMove;

            if (isValidHorizontalMove(newX)) currentX = newX;
            if (isValidVerticalMove(newY)) currentY = newY;
        }
        board[currentY][currentX] = 'X';
        printBoard(board);
    }

    /**
     * Método que recibe los movimientos que se harán
     * @return arreglo de movimientos
     */
    public static int[] inputMoves() {
        Scanner scanner = new Scanner(System.in);

        // Validar para que el número de movimientos sea par
        System.out.print("Ingrese el número de movimientos: ");
        int movesInt = scanner.nextInt();
        while (movesInt % 2 != 0) {
            System.out.println("El número de movimientos debe ser par");
            System.out.print("Ingrese el número de movimientos: ");
            movesInt = scanner.nextInt();
        }

        int[] moves = new int[movesInt];
        for (int i = 0; i < movesInt; i++) {
            System.out.print("Ingrese el movimiento " + (i + 1) + ": ");
            moves[i] = scanner.nextInt();
        }
        return moves;
    }

    /**
     * Método que crea el tablero 4x4
     *
     * @return tablero
     */
    public static char[][] createChessBoard() {
        char[][] board = {
                {'O', 'O', 'O', 'O'},
                {'O', 'O', 'O', 'O'},
                {'O', 'O', 'O', 'O'},
                {'O', 'O', 'O', 'O'}
        };
        return board;
    }

    /**
     * Método que valida si un movimiento horizontal es válido
     *
     * @param x posición x
     * @return true si el movimiento es válido, false en caso contrario
     */
    public static boolean isValidHorizontalMove(int x) {
        return x >= 0 && x <= 3;
    }

    /**
     * Método que valida si un movimiento vertical es válido
     *
     * @param y posición y
     * @return true si el movimiento es válido, false en caso contrario
     */
    public static boolean isValidVerticalMove(int y) {
        return y >= 0 && y <= 3;
    }

    /**
     * Método que imprime un tablero de ajedrez
     *
     * @param board tablero
     */
    public static void printBoard(char[][] board) {
        for (char[] row : board) {
            System.out.println(Arrays.toString(row));
        }
    }

}
