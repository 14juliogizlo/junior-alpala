package ec.telconet.prueba.Junior.Alpala.services;

import ec.telconet.prueba.Junior.Alpala.dao.UserRepository;
import ec.telconet.prueba.Junior.Alpala.models.InformacionUsuario;
import ec.telconet.prueba.Junior.Alpala.models.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final InformacionUsuarioService informacionUsuarioService;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = userRepository.findByUsuario(username)
                .orElseThrow(() -> new UsernameNotFoundException("No se encontró el nombre de usuario: " + username));

        InformacionUsuario informacionUsuario = informacionUsuarioService.findByUsuario(usuario);

        return UserDetailsImpl.build(usuario, Set.of(informacionUsuario.getRol()));
    }
}