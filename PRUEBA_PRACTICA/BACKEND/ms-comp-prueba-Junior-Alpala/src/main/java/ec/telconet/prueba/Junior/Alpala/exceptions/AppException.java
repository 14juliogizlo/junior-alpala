package ec.telconet.prueba.Junior.Alpala.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private HttpStatus status;
    private String mensaje;

    public AppException(HttpStatus status, String mensaje) {
        super(mensaje);
        this.status = status;
        this.mensaje = mensaje;
    }

}