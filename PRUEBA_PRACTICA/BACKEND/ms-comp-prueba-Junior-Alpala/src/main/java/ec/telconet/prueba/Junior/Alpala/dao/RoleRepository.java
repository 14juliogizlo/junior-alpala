package ec.telconet.prueba.Junior.Alpala.dao;

import ec.telconet.prueba.Junior.Alpala.models.ERole;
import ec.telconet.prueba.Junior.Alpala.models.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Rol, Long> {

    Optional<Rol> findByNombre(ERole name);
}