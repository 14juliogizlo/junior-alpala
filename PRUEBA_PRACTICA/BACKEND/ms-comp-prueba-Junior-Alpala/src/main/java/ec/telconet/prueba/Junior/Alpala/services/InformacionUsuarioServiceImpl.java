package ec.telconet.prueba.Junior.Alpala.services;

import ec.telconet.prueba.Junior.Alpala.dao.InformacionUsuarioRepository;
import ec.telconet.prueba.Junior.Alpala.models.InformacionUsuario;
import ec.telconet.prueba.Junior.Alpala.models.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InformacionUsuarioServiceImpl implements InformacionUsuarioService {

    private final InformacionUsuarioRepository informacionUsuarioRepository;

    @Override
    public InformacionUsuario save(InformacionUsuario informacionUsuario) {
        return informacionUsuarioRepository.save(informacionUsuario);
    }

    @Override
    public InformacionUsuario findByUsuario(Usuario usuario) {
        return informacionUsuarioRepository.findByUsuario(usuario);
    }

}
