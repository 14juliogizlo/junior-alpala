create table if not exists admi_rol(
   id 			serial 			primary key 	not null,
   nombre		varchar(200),
   descripcion varchar(200)
);

create table if not exists admi_usuario(
   id 			serial 			primary key 	not null,
   usuario				varchar(200),
   password 			varchar(200),
   nombres				varchar(200),
   apellidos			varchar(200),
   direccion			varchar(200),
   telefono			    varchar(200),
   usuario_creacion	    varchar(200),
   fecha_creacion		date,
   foto				bytea
);

create table if not exists info_usuario_rol (
    id 						serial 			primary key 	not null,
    usuario_id				integer,
    rol_id					integer,
    usuario_creacion 		varchar(200),
    fecha_creacion			date,
    usuario_modificacion	varchar(200),
    fecha_modificacion		date,
    estado					bool,
    foreign key (usuario_id) references admi_usuario(id),
    foreign key (rol_id)		references admi_rol(id)
);