package ec.telconet.prueba.Junior.Alpala.controllers;

import ec.telconet.prueba.Junior.Alpala.dao.RoleRepository;
import ec.telconet.prueba.Junior.Alpala.dao.UserRepository;
import ec.telconet.prueba.Junior.Alpala.exceptions.AppException;
import ec.telconet.prueba.Junior.Alpala.models.ERole;
import ec.telconet.prueba.Junior.Alpala.models.InformacionUsuario;
import ec.telconet.prueba.Junior.Alpala.models.Rol;
import ec.telconet.prueba.Junior.Alpala.models.Usuario;
import ec.telconet.prueba.Junior.Alpala.payload.request.LoginRequest;
import ec.telconet.prueba.Junior.Alpala.payload.request.SignupRequest;
import ec.telconet.prueba.Junior.Alpala.payload.response.JwtResponse;
import ec.telconet.prueba.Junior.Alpala.payload.response.MessageResponse;
import ec.telconet.prueba.Junior.Alpala.security.jwt.JwtUtils;
import ec.telconet.prueba.Junior.Alpala.services.InformacionUsuarioService;
import ec.telconet.prueba.Junior.Alpala.services.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final InformacionUsuarioService informacionUsuarioService;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;

    @Value("${api.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(
                userDetails.getId(),
                userDetails.getUsername(),
                jwt,
                jwtExpirationMs,
                roles));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsuario(signUpRequest.getUsuario())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: ¡El nombre de usuario ya está en uso!"));
        }
        // Crear nueva cuenta de usuario
        Usuario usuario = new Usuario(
                signUpRequest.getUsuario(),
                encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getNombres(),
                signUpRequest.getApellidos(),
                signUpRequest.getDireccion(),
                signUpRequest.getTelefono(),
                signUpRequest.getUsuarioCreacion(),
                signUpRequest.getFoto()
        );

        String role = signUpRequest.getRol();
        ERole erole = null;

        if (role.equals("ADMINISTRADOR")) erole = ERole.ADMINISTRADOR;
        if (role.equals("EDITOR")) erole = ERole.EDITOR;
        if (role.equals("CONSULTOR")) erole = ERole.CONSULTOR;

        Rol roleDb = roleRepository.findByNombre(erole).orElseThrow(
                () -> new AppException(HttpStatus.NOT_FOUND, "Error: No se encuentra el rol.")
        );

        InformacionUsuario informacionUsuario = new InformacionUsuario(
                roleDb,
                usuario,
                signUpRequest.getUsuarioCreacion(),
                signUpRequest.getUsuarioModificacion(),
                true
        );

        informacionUsuarioService.save(informacionUsuario);

        return ResponseEntity.ok(new MessageResponse("¡Usuario registrado con éxito!"));
    }

}