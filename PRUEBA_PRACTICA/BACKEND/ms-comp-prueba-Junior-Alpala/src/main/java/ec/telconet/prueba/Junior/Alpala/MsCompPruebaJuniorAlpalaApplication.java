package ec.telconet.prueba.Junior.Alpala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaJuniorAlpalaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsCompPruebaJuniorAlpalaApplication.class, args);
    }

}
