package ec.telconet.prueba.Junior.Alpala.payload.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class SignupRequest {

    @NotNull(message = "El campo no puede estar vacío")
    private String usuario;

    @NotNull(message = "El campo no puede estar vacío")
    private String password;

    @NotNull(message = "El campo no puede estar vacío")
    private String nombres;

    @NotNull(message = "El campo no puede estar vacío")
    private String apellidos;

    @NotNull(message = "El campo no puede estar vacío")
    private String direccion;

    @NotNull(message = "El campo no puede estar vacío")
    private String telefono;

    @NotNull(message = "El campo no puede estar vacío")
    private String usuarioCreacion;

    @NotNull(message = "El campo no puede estar vacío")
    private String usuarioModificacion;

    private byte[] foto;

    @NotNull(message = "El campo no puede estar vacío")
    private String rol;


}