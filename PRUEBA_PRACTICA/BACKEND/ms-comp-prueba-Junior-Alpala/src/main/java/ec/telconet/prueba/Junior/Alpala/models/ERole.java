package ec.telconet.prueba.Junior.Alpala.models;

public enum ERole {
    ADMINISTRADOR,
    EDITOR,
    CONSULTOR
}
