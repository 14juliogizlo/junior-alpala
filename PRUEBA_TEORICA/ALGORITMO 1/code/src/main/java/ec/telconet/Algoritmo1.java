package ec.telconet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Algoritmo1 {

    public static void main(String[] args) {
        int[] myArray = generateRandomArray(10);
        countConsecutiveNumbers(myArray);
    }

    /**
     * Método que recibe un arreglo de enteros e imprime el número que más se repite
     *
     * @param myArray arreglo de enteros
     * @author Alpala Saúl
     */
    public static void countConsecutiveNumbers(int[] myArray) {
        Map<Integer, Integer> concurrences = new HashMap<>();
        if (myArray.length != 0) {
            System.out.println(Arrays.toString(myArray));
            int numberWithMaxConcurrence = myArray[0];
            for (int number : myArray) {
                if (concurrences.containsKey(number)) {
                    int count = concurrences.get(number);
                    concurrences.put(number, count + 1);
                    if (count > concurrences.get(numberWithMaxConcurrence)) {
                        numberWithMaxConcurrence = number;
                    }
                } else {
                    concurrences.put(number, 1);
                }
            }
            System.out.println("Recurrencias: " + concurrences.get(numberWithMaxConcurrence));
            System.out.println("Número: " + numberWithMaxConcurrence);
        }
    }

    /**
     * Método que genera un arreglo de enteros aleatorios
     *
     * @param size tamaño del arreglo
     * @return arreglo Random de número enteros de tamaño n
     */
    public static int[] generateRandomArray(int size) {
        int[] randomArray = new int[size];
        for (int i = 0; i < size; i++) {
            randomArray[i] = (int) (Math.random() * 10);
        }
        return randomArray;
    }

}
