package ec.telconet.prueba.Junior.Alpala.payload.response;

import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {

    private int id;
    private String username;
    private String type = "Bearer";
    private String token;
    private Integer expirationIn;
    private List<String> roles;

    public JwtResponse(int id, String username, String accessToken, Integer expirationIn, List<String> roles) {
        this.id = id;
        this.username = username;
        this.token = accessToken;
        this.roles = roles;
        this.expirationIn = expirationIn;
    }
}