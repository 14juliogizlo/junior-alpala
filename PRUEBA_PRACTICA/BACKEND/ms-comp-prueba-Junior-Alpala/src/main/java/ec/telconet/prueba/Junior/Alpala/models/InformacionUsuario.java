package ec.telconet.prueba.Junior.Alpala.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "info_usuario_rol")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformacionUsuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "rol_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Rol rol;

    @OneToOne
    @JoinColumn(name = "usuario_id")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Usuario usuario;

    @Column(length = 200, name = "usuario_creacion")
    private String usuarioCreacion;

    @CreationTimestamp
    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion;

    @Column(length = 200, name = "usuario_modificacion")
    private String usuarioModificacion;

    @UpdateTimestamp
    private LocalDate fechaModificacion;

    @Column
    private Boolean estado;

    public InformacionUsuario(Rol rol, Usuario usuario, String usuarioCreacion, String usuarioModificacion, Boolean estado) {
        this.rol = rol;
        this.usuario = usuario;
        this.usuarioCreacion = usuarioCreacion;
        this.usuarioModificacion = usuarioModificacion;
        this.estado = estado;
    }
}