package ec.telconet.prueba.Junior.Alpala.dao;

import ec.telconet.prueba.Junior.Alpala.models.InformacionUsuario;
import ec.telconet.prueba.Junior.Alpala.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InformacionUsuarioRepository extends JpaRepository<InformacionUsuario, Long> {

    InformacionUsuario findByUsuario(Usuario usuario);

}
